const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors')

const app = express()
const port = 3333

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
jsonParser = bodyParser.json()

textToSpeech = require('./tts').textToSpeech

app.get('/', (req, res) => {
    res.send('TTS server up and running!')
})

app.post('/tts', (req, res) => {
    const text = req.body.text
    if (text) {
        console.log(`Requested TTS for: "${text}"`)
        const tts = textToSpeech(text)
        console.log(tts)
        switch(tts.type) {
            case 'local': 
                console.log(tts.path)
                res.sendFile(tts.path)
                break
            case 'dynamic':
                tts.promise.then((path)=>{
                    console.log(path)
                    res.sendFile(path)
                })
                break
            default:
                console.warn('Unacceptable tts type')
                break
          }
    } else res.status(400).send('"text" parameter required')
})

app.listen(port, (err) => {
    if (err) {
      return console.log('Something bad happened', err)
    }
  
    console.log(`Server is listening on ${port}`)
})