const fetch = require('node-fetch');

const fs = require('fs');

const token = require('./key.json')["iam-token"]
const folderId = require('./key.json')["folder-id"]
const ttsSettings = require('./ttsSettings.json');
const { setTimeout } = require('timers');

// Caching system
const ttsPairs = new Map(require('./oggs.json'))

const rewritePairs = () => {
    fs.writeFile('./oggs.json', JSON.stringify(Array.from(ttsPairs), null, 2), (e)=>{
        console.warn(e)
    });
}

const addToPairs = (id, text) => {
    ttsPairs.set(text, id)
    rewritePairs()
    console.log(`Successfully saved new sound for: "${text}"`)
}

const pathByID = (id) => __dirname + '/sounds/' + id + '.ogg'
const pathByText = (text) => pathByID(ttsPairs.get(text))

const getNewSoundNumber = () => {
    let exists = true;
    let number = 1;

    while (exists) {
        if (fs.existsSync(pathByID(number))) {
            number++;
        } else {
            return number
        }
    }
}


// Settings for TTS fetch 
const URL = 'https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize'

const params = new URLSearchParams();
for (setting in ttsSettings) {
    params.append(setting, ttsSettings[setting]);
}
params.append('folderId', folderId);


// Get TTS sound through fetch api. Returns Promise that resolves path to file
const createNewSound = async (text) => {
    console.log(`Creating new sound for: "${text}"`)
    params.set('text', text);
    
    try {
        const res = await fetch(URL, {
            method: 'post',        
            body: params,
            headers: { 
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + token,
            },
        })
        console.log(`Successfully created new sound for: "${text}"`)
        let id = getNewSoundNumber()
        const dest = fs.createWriteStream(pathByID(id));
        res.body.pipe(dest);
        setTimeout(()=>{
            addToPairs(id, text)
        })
        console.log('saved')
        const promise = new Promise((resolve) => dest.on('finish', ()=>{
            resolve(pathByID(id))
        }))
        return promise
    } catch (err) {
        console.error(err)
    }
}

const textToSpeech = (text) => {
    if (ttsPairs.has(text)) {
        console.log(`Found cached sound for: "${text}"`)
        return {type: 'local', path: pathByText(text)}
    } else {
        return {type: 'dynamic', promise: createNewSound(text)}
    }
}

module.exports.textToSpeech = textToSpeech